<?php

namespace Config\Local\DefaultApp;

/**
 * Class Misc
 * @package Config\Local\DefaultApp
 */
class Misc
{
    /**
     * @var string Lock Key Prefix
     */
    public $geoIpDB = '';
}